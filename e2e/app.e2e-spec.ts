import { InteraktivePage } from './app.po';

describe('interaktive App', () => {
  let page: InteraktivePage;

  beforeEach(() => {
    page = new InteraktivePage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});

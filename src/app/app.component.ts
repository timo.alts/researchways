import {Component} from '@angular/core';
import {DataService} from './service/data/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  discover = false;

  currentcategory = '';

  constructor(private datas: DataService) {
    datas.selectedCategory.subscribe(c => this.currentcategory = c);
  }

  search(a: string) {
    this.datas.search(a);
  }

  setCategory(a: string) {
    this.datas.setCategory(a);
  }

  setTopTen() {
    this.datas.setTop10();
  }

  setAll() {
    this.datas.setall();
  }
}

import {Component, OnInit} from '@angular/core';
import {DataService} from '../service/data/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  displaydata = [];

  constructor(private data: DataService) {
    data.content.subscribe(d => this.displaydata = d);
  }

  ngOnInit() {
  }

}

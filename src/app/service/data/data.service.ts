import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class DataService {
  public content: BehaviorSubject<Array<any>> = new BehaviorSubject<Array<any>>(null);
  public categorycontent: BehaviorSubject<Array<any>> = new BehaviorSubject<Array<any>>(null);
  public selectedCategory: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public searchKeyword: BehaviorSubject<string> = new BehaviorSubject<string>(null);


  array = [
    {
      'title': '5 Why\'s Interview',
      'description_short': 'Ask "Why?" 5 times to get to root concern of participant.',
      'category': [
        'define'
      ]
    }, {
      'title': 'A (x4) Model Observation',
      'description_short': 'Atmosphere, Actors, Artifacts, Activities',
      'category': [
        'discover'
      ]
    }, {
      'title': '9D Obeservation',
      'description_short': 'Observation frame: Space, Actors, Activities, Objects, Acts, Events, Time, Goals, Feelings.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'ABA Design',
      'description_short': 'Participant(s) engage in a baseline experience state, followed by a new state, and then they revert to the original baseline state. Alternatively, deliver two different experiences to guage reactions.',
      'category': [
        'evaluate'
      ]
    }, {
      'title': 'AEOUT Observation',
      'description_short': 'Observational frame from the Doblin group that looks at the intersection of key elements: AEOUT: Activities, Environments, Objects, Users, Time.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Appreciative Inquiry',
      'description_short': 'Focus on the positive attributes and possibilities of a collectivity through 4 conversations: discovery (appreciating the best of what is), dream (envisioning what could be), design (co-constructing what should be), and destiny (sustaining what will be).',
      'category': [
        'define',
        'conceptualize'
      ]
    }, {
      'title': 'Appreciative Inquiry: Business Planning Using SOAR',
      'description_short': '',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Argumentation Analysis',
      'description_short': '\u201cThe aim of argumentation analysis is to document the manner in which statements are structured within a discursive text, and to assess their soundness. The analysis usually centres on the interaction between two or more people who argue as a ...',
      'category': [
        'conceptualize'
      ]
    }, {
      'title': 'Backcasting',
      'description_short': '',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Behavioural Mapping',
      'description_short': '',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Bilbliometrics',
      'description_short': '\u201cA method based on quantitative and statistical analysis of publications. This may involve simply charting the number of publications emerging in an area, perhaps focusing on the outputs from different countries in different fields and how they are evolving over ...',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Bodystorming (Participatory Workshop)',
      'description_short': 'Designing in context and may involve role-playing and improv with scenarios or problem challenges.',
      'category': [
        'discover',
        'define',
        'conceptualize',
        'design'
      ]
    }, {
      'title': 'Brainstorming',
      'description_short': 'Structured, rapid process to generate new ideas around a specific area of interest, e.g. SCAMPER, or Mind Mapping techniques. Ideas are not critiqued in the process but are vetted after generation.',
      'category': [
        'discover',
        'define',
        'conceptualize'
      ]
    }, {
      'title': 'Bringing the Outside in (Observation)',
      'description_short': 'Observation frame: Territory (space and architecture), Stuff (furniture, possessions, visual information, technology), People (behaviour, norms), Talk (conversation, vocabularies).',
      'category': [
        'discover',
        'define',
        'conceptualize',
        'design',
        'implement',
        'evaluate'
      ]
    }, {
      'title': 'Business Model Canvas',
      'description_short': 'This tool supports the process of business modeling from marketing and strategy to key partnerships etc.',
      'category': [
        'conceptualize',
        'design',
      ]
    }, {
      'title': 'Card Sorting',
      'description_short': 'On separate cards, participants identify relevant issues (possibly features, functions, or design attributes). Then, they are asked to group or organize the cards into meaningful categories or hierarchies.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Character Profiles',
      'description_short': 'See Personas',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Citizen\'s Panel',
      'description_short': '\u201cA method that brings together groups of citizens (members of a polity and\/or residents of a particular geographic area) dedicated to providing views on relevant issues, often for a regional or national government).\u201d',
      'category': [
        'define'
      ]
    }, {
      'title': 'Cognitive Task Analysis (Mental model elicitation)',
      'description_short': 'This is a task-specific usability testing technique where users\u2019 descriptions of the steps involved in performing a task can be compared to the interface\/system to evaluate areas for improvement.',
      'category': [
        'discover',
        'define',
        'conceptualize',
        'design',
        'implement',
        'evaluate'
      ]
    }, {
      'title': 'Cognitive Walkthrough (Mental model elicitation)',
      'description_short': 'This is a usability testing technique that is intended to evaluate a design\u2019s \u201cease of learning\u201d by following a user\u2019s experience as he\/she explores an interface\/system. It suggests areas for improvement and often involves the narration technique.',
      'category': [
        'design'
      ]
    }, {
      'title': 'Cognitive Work Analysis (Mental model elicitation)',
      'description_short': '\u201cCognitive Work Analysis is a conceptual framework that makes it possible to analyse the forces that shape human-information interaction. Its approach is work-centred, rather than user-centred, as it analyses the constraints and goals that shape information behaviour in the work ...',
      'category': [
        'conceptualize'
      ]
    }, {
      'title': 'Competitive Analysis',
      'description_short': 'Compare and evaluate existing, competing and \/ or complementary offerings. Can also include a review of existing patents or technologies in terms of features, functions, and performance.',
      'category': [
        'discover',
        'define',
        'conceptualize'
      ]
    }, {
      'title': 'Concept Mapping (Mental model elicitation)',
      'description_short': 'A visual mapping of relationships between various ideas, images, and words. Concept maps are interpretive clusters. Mind Maps are generative ideation tools and begin with the topic at the centre.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Content Analysis',
      'description_short': 'Content analysis (\u201cCA\u201d) is a text-based form of analysis, covering data ranging from newspapers to office memos, to poster advertisements. The technique involves researchers making inferences on social context based on text data (e.g. values, rules, norms, conflict, etc.). It ...',
      'category': [
        'define'
      ]
    }, {
      'title': 'Contextual Design',
      'description_short': 'See think aloud and contextual inquiry.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Contextual Inquiry',
      'description_short': 'This method is used for gathering information on user experience in their \u201cnatural habitat\u201d when using a particular technology or performing a particular activity. Contextual inquiry involves both behavioural observation and contextual interviews while a user performs tasks etc. The ...',
      'category': [
        'discover',
        'define'
      ]
    }, {
      'title': 'Conversation Analysis',
      'description_short': '\u201cIt is concerned with how participants organize interaction from moment to moment; it does not seem to be concerned with the social structures, changes, attitudes, identities or groups studied in other social science approaches.\u201d Some features for analysis: Sequence: throughout conversations, there ...',
      'category': [
        'discover',
        'define',
        'conceptualize',
        'design',
        'implement',
        'evaluate'
      ]
    }, {
      'title': 'Critical Discourse Analysis',
      'description_short': 'Critical discourse analysis involves \u201ca detailed systematic examination of a particular object with a view to arrive at one or more underlying principles,\u201d examines language and \u201cthe way in which patterns of meaning are socially constructed,\u201d and considers the \u201csocial ...',
      'category': [
        'define'
      ]
    }, {
      'title': 'Cross-Impact\/Structural Analysis',
      'description_short': '\u201cA method that works systematically through the relations between a set of variables, rather than examining each one as if it is relatively independent of the others. Usually, expert judgement is used to examine the influence of each variable within ...',
      'category': [
        'discover',
        'define',
        'conceptualize'
      ]
    }, {
      'title': 'Cultural Probes',
      'description_short': 'Participants are given kits for self-documentation of relevant issues or experiences. Kits may include disposable cameras, maps, journals and postcards.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Deductive Analysis',
      'description_short': 'Identification of themes and patterns by applying predetermined criteria or concepts, themes, or coding system(s) to the data, which may or may not have been sorted into smaller groups. See Content Analysis, Program Evaluation, Rhetorical Analysis.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Delphi',
      'description_short': 'Repeated polling of the same individuals, feeding back anonymised (summary) responses from earlier rounds of polling, to arrive at consensus (or determine entrenched differences).',
      'category': [
        'define',
        'conceptualize',
        'design'
      ]
    }, {
      'title': 'Design Documentaries',
      'description_short': 'This technique involves using manipulated documentary footage (e.g. with additional imagery or manipulated scenarios) to inspire creative discussion within a design team.',
      'category': [
        'conceptualize',
        'design',
        'implement'
      ]
    }, {
      'title': 'Design Fiction',
      'description_short': 'The creation of stimulous, fictional artifacts or experiences to guage responses or open up possibilities for future states. It often combines design, science fact, and science fiction.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Design the Box',
      'description_short': '',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Design Games',
      'description_short': 'This method can be used to facilitate collaborative and participatory design efforts by using the structure and rules of game experiences.',
      'category': [
        'conceptualize',
        'design'
      ]
    }, {
      'title': 'Design Probes',
      'description_short': 'Similar to cultural probes, design probes allow for participant self-documentation of experiences, events, etc., however, often in connection with a more focused scope, perhaps around a specific product design or concept.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Dialogic Design',
      'description_short': '\u201cDesign Dialogues imagines the possibilities of design as a transformative revisioning of systems that matter. We require new tools of design thinking and social engagement to energize the wisdom of participants. Dialogue is between perspectives, around a multi-perspective design canvas ...',
      'category': [
        'discover',
        'define',
        'conceptualize',
        'design'
      ]
    }, {
      'title': 'Discourse Analysis',
      'description_short': 'There are at least 57 types of discourse analysis, which share a commonality in that they involve the study of texts, whereby the language comprising the text is analyzed to gain insight into social life. (See description of textual analysis ...',
      'category': [
        'define'
      ]
    }, {
      'title': 'Empathy Tools',
      'description_short': 'The creation and use of tools to simulate the experience of a range of users.',
      'category': [
        'discover',
        'define',
        'conceptualize',
        'design'
      ]
    }, {
      'title': 'Environmental Scanning',
      'description_short': '\u201cA method that involves observation, examination, monitoring and systematic description of the social, technological, economic, environmental, political and ethical contexts of a country, industry, organisation, etc.\u201d',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Essays',
      'description_short': '\u201cA method focused on one or a small set of images of the future, with a detailed description of some major trends promoting the evolution of a particular scenario, and\/or of stakeholders\u2019 roles in helping to bring these about).\u201d',
      'category': [
        'define'
      ]
    }, {
      'title': 'Expert Interviews',
      'description_short': 'Interviews with experts in the field (can supplement literature reviews).',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Expert Panel',
      'description_short': 'An assembly of experts to discuss a given topic, service, or design. It may also be used to help understand and rank the impact of factors.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Extreme User Interviews',
      'description_short': 'Interviews with lead users and non-users or those with physical impairments',
      'category': [
        'discover',
        'define'
      ]
    }, {
      'title': 'Focus Groups',
      'description_short': 'A facilitated gathering of groups of people for discussion on opinions and experiences.',
      'category': [
        'define',
        'conceptualize',
        'design',
        'implement',
        'evaluate'
      ]
    }, {
      'title': 'Foresight',
      'description_short': 'See scenario planning, wildcards, futures workshop',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Future Technology Workshop',
      'description_short': 'This method involves a series of seven workshops intended to reduce biases and facilitate the envisioning of future technologies and technology-mediated activities or interactions.',
      'category': [
        'conceptualize'
      ]
    }, {
      'title': 'Future Workshop (Foresight)',
      'description_short': '\u201cA method that involves the organisation of events or meetings lasting from a few hours to a few days, in which there is typically a mix of talks, presentations, and discussions and debates on a particular subject.\u201d',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Generative Research',
      'description_short': 'See Make Tools (other techniques for further reading are projective collaging and critical making)',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Grounded Theory',
      'description_short': 'Originally developed by Barney Glaser and Anselm Strauss, grounded theory involves a systematic analysis of data to reveal emergent insights, similar to thematic analysis.',
      'category': [
        'conceptualize',
        'design'
      ]
    }, {
      'title': 'Heuristics',
      'description_short': 'Set of principles or rules used to evaluate the usability of graphical user interfaces, usually developed by \u201cexperts\u201d but may be developed from field research.',
      'category': [
        'discover',
        'define',
        'conceptualize',
        'design'
      ]
    }, {
      'title': 'Historical Analysis',
      'description_short': 'Overview analysis to see tech trends, how quickly new types of interfaces were introduced\/adapted (e.g. from the mouse and keyboard to touch screen to smart surfaces etc.)',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Horizon Scan',
      'description_short': 'A systematic process to identify and monitor changes in the contextual environment. Typical frameworks for inquiry, e.g. STEEP+V: science, technology, economic, environmental, political and values.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'In-Depth Interview',
      'description_short': 'The classic two-hour, in-context, in-depth interview. It is usually semi-structured and uncovers the relavant issues for the participant.',
      'category': [
        'discover',
        'define'
      ]
    }, {
      'title': 'Inductive Analysis',
      'description_short': 'Analyzing the data as a whole to identify any emergent patterns or themes. This approach is more open-ended and discovery oriented than deductive analysis. See Argumentation Analysis, Conversation Analysis, Critical Discourse Analysis, Discourse Analysis, Grounded Theory, Semiotic Analysis, Textual Analysis, ...',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Instrumenting',
      'description_short': 'This technique involves equipping a system with means to measure and capture data on its use and performance.',
      'category': [
        'evaluate'
      ]
    }, {
      'title': 'Intercept Interviews',
      'description_short': 'Opportunistic, quick interviews conducted at the moment of action or engagement by the participant.',
      'category': [
        'define',
        'conceptualize',
        'design',
        'implement',
        'evaluate'
      ]
    }, {
      'title': 'Interviews',
      'description_short': 'See 5 Why\u2019s, Expert Interview, Extreme User Interviews, Intercept Interviews, the Long Interview, Open-Ended Interview, Semi-Structured Interview, Sense-Making Interview',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Key Technologies',
      'description_short': '\u201cA method that involves the elaboration of a list of key technologies for a specific industry, country or region. A technology is said to be \u2018 \u2018key\u2019 \u2019 if it contributes to wealth creation or if it helps to increase ...',
      'category': [
        'discover',
        'define'
      ]
    }, {
      'title': 'Lead User Research',
      'description_short': 'Research with avid or extreme users in order to identify potential opportunities to innovate. Lead users may experience needs that do not yet exist in the broader market.',
      'category': [
        'discover',
        'define'
      ]
    }, {
      'title': 'Literature Review',
      'description_short': 'Reviews key issues, thinking and tensions in a given field.',
      'category': [
        'discover',
        'define',
        'conceptualize',
        'design',
        'implement',
        'evaluate'
      ]
    }, {
      'title': 'The Long Interview',
      'description_short': 'The classic two-hour, in-context, in-depth interview.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Longitudinal Studies',
      'description_short': 'This form of research is often observational and involves \u201ctracing the experience of the same individuals over time.\u201d Through this approach to research, better understandings of cause and effect may emerge, with the potential for informing long-term planning and policy ...',
      'category': [
        'evaluate'
      ]
    }, {
      'title': 'Make Tools (Generative research)',
      'description_short': 'Co-design sessions with participants, designers and researchers to reveal shared experiences and viewpoints (e.g., dreams, fears, insights, opportunities). This usually results in physical, tangible artifacts.',
      'category': [
        'define',
        'conceptualize',
      ]
    }, {
      'title': 'Megatrend Analysis',
      'description_short': '\u201cAmong the longest-established tools of forecasting. They provide a rough idea of how past and present developments may look like in the future \u2013 assuming, to some extent, that the future is a continuation of the past.\u201d',
      'category': [
        'discover',
        'define',
      ]
    }, {
      'title': 'Mental Model Elicitation',
      'description_short': 'See Concept Mapping, Cognitive Task Analysis',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Mobile Diaries',
      'description_short': 'Using mobile devices, online blogs or paper booklets for daily reflections on experiences.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Modelling and Simulation',
      'description_short': '\u201cA method that refers to the use of computer-based models that relate together the values achieved by particular variables. Simple models may be based on statistical relations between two or three variables only. More complex models may use hundreds, thousands, ...',
      'category': [
        'discover',
        'define',
        'conceptualize',
        'design',
        'implement',
        'evaluate'
      ]
    }, {
      'title': 'Morphological Analysis',
      'description_short': '\u201cA method used to map promising solutions to a given problem and to determine possible futures accordingly. It is generally used to suggest new products or developments and to build multi-dimensional scenarios.\u201d',
      'category': [
        'conceptualize',
      ]
    }, {
      'title': 'Multi-Criteria Analysis',
      'description_short': '\u201cA method used as prioritisation and decision-support technique, especially in complex situations and problems, where there are multiple criteria in which to weigh up the effect of a particular intervention.\u201d',
      'category': [
        'conceptualize',
        'design'
      ]
    }, {
      'title': 'Narration', 'description_short': 'See Think Aloud',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Needs Assessment',
      'description_short': 'This technique focuses on comparing current results with existing results to determine actual needs.',
      'category': [
        'conceptualize',
        'design'
      ]
    }, {
      'title': 'Nominal Group Technique',
      'description_short': 'This technique supports group decision making through a five-stage workshop starting with an introduction to provide context to the discussion, then participants individually record their opinions on the given discussion topic, then ideas are shared with the group by the ...',
      'category': [
        'discover',
      ]
    }, {
      'title': 'Open-Ended Interview',
      'description_short': 'Most exploratory format of interviews. Good for gaining deep understanding of user and user\u2019s context. Can provide foundation and directions for further research.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Participatory Workshop',
      'description_short': 'See Appreciative Inquiry, Bodystorming, Role-Playing, World Caf\u00e9',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Personas',
      'description_short': 'Profiles: standalone descriptions of participants according to relevant behaviours, attitudes and painpoints. Personas are amalgamated, interpretive descriptions of these.',
      'category': [
        'discover',
        'define',
        'conceptualize'
      ]
    }, {
      'title': 'Photovoice', 'description_short': '',
      'category': [
        'discover'
      ]
    }, {
      'title': 'PICTIVE',
      'description_short': 'PICTIVE is an acronym for Plastic Interface for Collaborative Technology Initiative through Video Exploration. It is a technique that allows users to participate in developing graphical user interfaces, often through simple paper materials, and was originally developed at Bellcore circa ...',
      'category': [
        'design'
      ]
    }, {
      'title': 'Playful Triggers',
      'description_short': 'Reflective probes, primitive probes, playful triggers',
      'category': [
        'discover'
      ]
    }, {
      'title': 'POEMS Observation',
      'description_short': 'Structured or unstructured "watching" of a space, activity or event. Researcher may participate or be a fly on the wall, depending on the situation.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'POSTA Observation',
      'description_short': 'Person, Objects, Situations, Time, Activity',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Program Evaluation',
      'description_short': 'Program evaluation involves the assessment of the effectiveness or success of a series of activities or procedures. It is often recommended that measures of effectiveness or success be embedded into a given program during its design phase.',
      'category': [
        'evaluate'
      ]
    }, {
      'title': 'Prototyping',
      'description_short': 'Rough mock-ups of designs, ranging from wireframes, physical products to spatial experiences for gathering insight into fit, usefulness, usability, and desirability.',
      'category': [
        'define',
        'conceptualize',
        'design'
      ]
    }, {
      'title': 'Quality Assurance Testing',
      'description_short': 'This technique consists of a systematic process of testing a product, service, or system (e.g. software) to ensure a minimum level of performance on multiple dimensions.',
      'category': [
        'implement'
      ]
    }, {
      'title': 'Questionnaires',
      'description_short': 'Structured questions that are administered in person, online or through mail for comparability of results.',
      'category': [
        'discover',
        'define',
        'conceptualize',
        'design',
        'implement',
        'evaluate'
      ]
    }, {
      'title': 'Rapid Prototyping With CNC Tools', 'description_short': '',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Relevance Trees',
      'description_short': '\u201cA method in which the topic of research is approached in a hierarchical way. It normally begins with a general description of the subject, and continues with a disaggregated exploration of its different components and elements, examining particularly the interdependencies ...',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Repertory Grid',
      'description_short': 'This technique has been used to gain insight into how a participant constructs an understanding of a given experience. It maps four layers of understanding, consisting of the overarching topic (e.g. vision-impaired patient), elements or descriptors associated with the topic ...',
      'category': [
        'discover',
        'define'
      ]
    }, {
      'title': 'Rhetorical Analysis',
      'description_short': 'Rhetorical analysis involves research into the effect of various elements of discourse on an audience. It includes analysis of the type(s) of audience appeal: logos (logic), pathos (emotion), and\/or ethos (credibility of speaker). Rhetorical analysis also involves consideration for the ...',
      'category': [
        'define'
      ]
    }, {
      'title': 'Role-Playing (Participatory workshop)',
      'description_short': 'Create a scenario and have a group act it out with specific roles',
      'category': [
        'discover',
        'define',
        'conceptualize',
        'design'
      ]
    }, {
      'title': 'Scenario Planning (Foresight)',
      'description_short': 'Descriptions or stories of possible, multiple future worlds to help anticipate potential changes in the environment for design and strategic development aims.',
      'category': [
        'define'
      ]
    }, {
      'title': 'Secondary Research', 'description_short': '',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Semi-Structured Interviews',
      'description_short': 'Middle ground between exploratory and representative interview formats. Good for gaining some depth of understanding of user and user\u2019s context while providing comparability between results.',
      'category': [
        'discover',
        'define'
      ]
    }, {
      'title': 'Semiotic Analysis',
      'description_short': 'This method involves a systematic approach to discover how meaning is derived from signs, images, etc. The process typically involves dissection of the image, then articulation or reconstruction of the meaning embedded in the image, with the goal of revealing ...',
      'category': [
        'define'
      ]
    }, {
      'title': 'Sense-Making Interviews',
      'description_short': 'In sense-making, interview questions are intended to be as neutral as possible, starting with questions relating to situational-understanding, then gaps (or questions they have) relating to their situation, and then an understanding of how they plan to use the answers ...',
      'category': [
        'discover',
        'define'
      ]
    }, {
      'title': 'Shadowing',
      'description_short': 'Participant is followed, with minimal intrusion, through tasks or routines for interface design or experiential understanding.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Social Network Mapping',
      'description_short': 'Map of social relationships within a user group.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Stakeholder Matrix',
      'description_short': 'Stakeholders and their concerns are identified with regard to a particular challenge or initiative. This technique can be used to aggregate multiple viewpoints and identifying potential areas of conflicting interest.',
      'category': [
        'define'
      ]
    }, {
      'title': 'Standards and Guidelines',
      'description_short': 'This refers broadly to a predetermined set of requirements or suggestions for design decisions. One example would be Web Content Accessibility Guidelines (WCAG).',
      'category': [
        'design'
      ]
    }, {
      'title': 'Storyboarding',
      'description_short': 'A sequence of events and interactions are illustrated to allow for analysis of key events and to develop process or task flows within a system.',
      'category': [
        'define',
        'conceptualize'
      ]
    }, {
      'title': 'Storytelling',
      'description_short': 'Participant stories that help develop empathy from other stakeholders. These help illuminate the significance of casual, unobserved events and experiences. Often used to help illustrate complex concepts.',
      'category': [
        'discover',
        'define',
        'conceptualize',
        'design',
        'implement'
      ]
    }, {
      'title': 'Surveys',
      'description_short': 'A survey is administered when there is a clear idea of the important questions to ask participants and a sense of all possible answers for those questions. This technique is intended to be generalizable across a population and typically involves ...',
      'category': [
        'discover'
      ]
    }, {
      'title': 'SWOT Analysis',
      'description_short': 'An organization conducts an internal scan of strengths and weaknesses, as well as an external scan of opportunities and threats. It may be supplemented with TOWS or replaced by SOAR.',
      'category': [
        'discover',
        'define',
        'conceptualize'
      ]
    }, {
      'title': 'Technology Roadmapping',
      'description_short': '\u201cA method which outlines the future of a field of technology, generating a timeline for development of various interrelated technologies and (often) including factors like regulatory and market structures.\u201d',
      'category': [
        'discover',
        'define',
        'conceptualize'
      ]
    }, {
      'title': 'Textual Analysis',
      'description_short': 'There are many approaches to textual analysis. Overall, it is a way to gather data and understand how people make sense of the world through analysis of text, which is defined broadly by Alan McKee as \u201csomething we make meaning ...',
      'category': [
        'define'
      ]
    }, {
      'title': 'Thematic Analysis',
      'description_short': 'Thematic analysis delays judgment and interpretation of data until data has been fully coded. Its intent is to summarize and condense data through a coding system around themes, which allows for data reconstruction from participants\u2019 point of view as much ...',
      'category': [
        'define'
      ]
    }, {
      'title': 'Think Aloud',
      'description_short': 'Participants narrate their thoughts and feelings as they engage with the spatial interface or activity. It may be used during a review of video of the participant\'s activity ("anthropump").',
      'category': [
        'discover',
        'define',
        'conceptualize',
        'design',
        'implement'
      ]
    }, {
      'title': 'Unfocus Group',
      'description_short': 'An emergent group discussion or workshop with a wide range of users with varying degrees of knowledge or experiences. The topics and issues are developed by the participants.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Usability Testing',
      'description_short': 'Assessment of a user\'s ease-of-use, speed-of-use, accuracy in performing tasks, and emotional experience through field or lab observation studies.',
      'category': [
        'implement',
        'evaluate'
      ]
    }, {
      'title': 'Use-Case Analysis',
      'description_short': 'This technique involves gathering information from the user regarding different aims and tasks required in using a system.',
      'category': [
        'conceptualize'
      ]
    }, {
      'title': 'User Needs Mapping',
      'description_short': 'See personas and stakeholder matrix',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Video Ethnography (Observation)',
      'description_short': 'Mounted video captures activities over time without researcher being present.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Wildcards (Foresight)',
      'description_short': 'Development of a "what if" scenario and\/ or assessing the robustness of a strategy or offering based upon the occurance of future high-impact and low-probability events. E.g. internet outtage or terrorist attack.',
      'category': [
        'discover'
      ]
    }, {
      'title': 'Wind Tunneling',
      'description_short': 'Wind tunneling refers to the use of computer modeling\/simulation tools or physical testing apparatus that allows for analysis of aerodynamic effects on a product or prototype.',
      'category': [
        'implement'
      ]
    }, {
      'title': 'World Caf\u00e9 (Participatory workshop)',
      'description_short': 'Hosted, facilitated gathering of 4-5 tables of people (4 people per table) to discuss a shared question. Members of tables rotate throughout the processes to review and add to other conversational insights.',
      'category': [
        'discover',
        'define',
        'conceptualize'
      ]
    }];


  constructor() {
    this.content.next(this.array);
    this.categorycontent.next(this.array);
    this.searchKeyword.next('');
  }

  public search(val: string) {
    this.searchKeyword.next(val || '');
    this.content.next(this.categorycontent.getValue().filter(t => {
      return t.title.toLowerCase().indexOf(val.toLowerCase()) !== -1;
    }));
  }

  public setCategory(v: string) {
    if (this.selectedCategory.getValue() !== v) {
      this.selectedCategory.next(v);
      this.categorycontent.next(this.array.filter(c => {
        return c.category.includes(v);
      }))
    } else {
      this.selectedCategory.next('');
      this.categorycontent.next(this.array)
    }
    this.search(this.searchKeyword.getValue());
  }

  public setTop10() {
    const top = [];
    top.push(this.array.filter(t => {
      return (t.title.toLowerCase().indexOf('brainstorming') !== -1);
    })[0]);
    top.push(this.array.filter(t => {
      return (t.title.toLowerCase().indexOf('focus groups') !== -1);
    })[0]);
    top.push(this.array.filter(t => {
      return (t.title.toLowerCase().indexOf('make tools') !== -1);
    })[0]);
    top.push(this.array.filter(t => {
      return (t.title.toLowerCase().indexOf('modelling and simulation') !== -1);
    })[0]);
    top.push(this.array.filter(t => {
      return (t.title.toLowerCase().indexOf('prototyping') !== -1);
    })[0]);
    top.push(this.array.filter(t => {
      return (t.title.toLowerCase().indexOf('semi-structured interviews') !== -1);
    })[0]);
    top.push(this.array.filter(t => {
      return (t.title.toLowerCase().indexOf('swot analysis') !== -1);
    })[0]);
    top.push(this.array.filter(t => {
      return (t.title.toLowerCase().indexOf('think aloud') !== -1);
    })[0]);
    top.push(this.array.filter(t => {
      return (t.title.toLowerCase().indexOf('video ethnography') !== -1);
    })[0]);
    top.push(this.array.filter(t => {
      return (t.title.toLowerCase().indexOf('card sorting') !== -1);
    })[0]);
    this.content.next(top);
  }

  public setall() {
    this.content.next(this.array);
    this.selectedCategory.next('');
  }
}
